using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour {

	[SerializeField] float levelLoadDelay = 1f;
	[SerializeField] GameObject deathParticles;

	void OnTriggerEnter(Collider other)
	{
		StartDeathSequence();
	}

	 void StartDeathSequence()
	{
		deathParticles.SetActive(true);
		SendMessage("OnPlayerDeath");
		SendMessageUpwards("OnPlayerDeath");
		Invoke("restartLevel", 2f);
	}
	void restartLevel()
	{
		SceneManager.LoadScene(1);
	}
}
