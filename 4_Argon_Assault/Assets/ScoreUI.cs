using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {

	int score = 0;
	Text scoreText;
	int timeScore = 5;
	// Use this for initialization
	void Start () {
		scoreText = GetComponent<Text>();
		scoreText.text = score.ToString();
	}
	
	// Update is called once per frame
	public void scoreHit(int scorePerHit)
	{
		score = score + scorePerHit;
		scoreText.text = score.ToString();

	}

	public void scorePerTime()
	{
		score = score + timeScore;
		scoreText.text = score.ToString();

	}
}
