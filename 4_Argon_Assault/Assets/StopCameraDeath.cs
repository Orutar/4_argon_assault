using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopCameraDeath : MonoBehaviour {

	[SerializeField] Player player;
	[SerializeField] BetterWaypointFollower waypointFollower;
	

	// Update is called once per frame
	void OnPlayerDeath() {
		waypointFollower.routeSpeed = 2;


	}

}
