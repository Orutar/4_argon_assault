using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour {

	// Use this for initialization
	[Header("General")]
	[Tooltip("m^-1")][SerializeField] float xSpeed = 15f;
	[Tooltip("m^-1")] [SerializeField] float ySpeed = 15f;
	[SerializeField] GameObject[] guns; 

	[Header("Screen position based")]
	[SerializeField] float positionPitchFactor = -1.66f;
	[SerializeField] float positionYawFactor = 2f;

	[Header("Control Throw based")]

	[SerializeField] float controlPitchFactor = -10f;
	[SerializeField] float controlRollFactor = -10f;

	float xThrow;
	float yThrow;
	public bool isAlive = true;

	ScoreUI scoreBoard;


	// Update is called once per frame
	void Update ()
	{
		scoreBoard = FindObjectOfType<ScoreUI>();
		if (isAlive) {

			ProcessTranslation();
			ProcessRotation();
			ProcessFiring();
			if (Time.frameCount % 30 == 0) scoreBoard.scorePerTime();

		}
		

	}

	void OnPlayerDeath()    //CARE, called by string reference
	{
		isAlive = false;
	}



	void ProcessRotation()
	{
		float pitch = positionPitchFactor * transform.localPosition.y + yThrow * controlPitchFactor;
		float yaw = positionYawFactor * transform.localPosition.x;
		float roll = xThrow * controlRollFactor;

		transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
	}

	private void ProcessTranslation()
	{
		 xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
		 yThrow = CrossPlatformInputManager.GetAxis("Vertical");

		float xOffset = xSpeed * xThrow * Time.deltaTime;
		float rawNewXPos = transform.localPosition.x + xOffset;
		float campledXPos = Mathf.Clamp(rawNewXPos, -7, 7);  //x pos limit from the camera
		float yOffset = ySpeed * yThrow * Time.deltaTime;
		float rawNewYPos = transform.localPosition.y + yOffset;
		float campledYPos = Mathf.Clamp(rawNewYPos, -4.5f, 4.5f);  //y pos limit from the camera

		transform.localPosition = new Vector3(campledXPos, campledYPos, transform.localPosition.z);
	}

	 void ProcessFiring()
	{
		if (CrossPlatformInputManager.GetButton("Fire"))
		{

			ActivateGuns();
			
		}
		else
		{
			DeactivateGuns();
		}
	}

	void ActivateGuns()
	{
		foreach (GameObject gun in guns)
		{
			gun.SetActive(true);
		}
	}
	void DeactivateGuns()
	{
		foreach (GameObject gun in guns)
		{
			gun.SetActive(false);

		}
	}

}

