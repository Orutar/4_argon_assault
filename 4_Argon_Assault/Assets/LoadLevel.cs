using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadLevel : MonoBehaviour {

	// Use this for initialization
	void Awake() {
		Invoke("LoadNextLevel", 2f);
		DontDestroyOnLoad(this.gameObject);

	}

	// Update is called once per frame
	void Update () {
	}

	void LoadNextLevel()
	{
		SceneManager.LoadScene(1);
	}
}

