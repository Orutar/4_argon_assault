using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	[SerializeField] GameObject DeathFX;
	[SerializeField] Transform parent;
	[SerializeField] int scorePerHit = 15;
	[SerializeField] int healthPoints = 10;


	ScoreUI scoreBoard;

	// Use this for initialization
	void Start () {


		Collider enemyCollider = gameObject.AddComponent<BoxCollider>();
		enemyCollider.isTrigger = false;
		scoreBoard = FindObjectOfType<ScoreUI>();
	}
	


	 void OnParticleCollision(GameObject other)
	{
		healthPoints -= 1;
		if (healthPoints <= 0) {
			scoreBoard.scoreHit(scorePerHit);
			KillEnemy();
		}
	}

	private void KillEnemy()
	{
		GameObject fx = Instantiate(DeathFX, transform.position, Quaternion.identity);
		fx.transform.parent = parent;
		Destroy(fx, 2f);
		Destroy(this.gameObject);
	}
}
